package com.guess.logo;

import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class ShadiEvents {
	public static void addShadiEvents(Schema schema) {
		Entity events = DBDaoGenerator.getEntity("ShadiEvents", schema);
		events.addStringProperty("id");
		events.addStringProperty("eventAddress");
		events.addStringProperty("eventDate");
		events.addStringProperty("eventLat");
		events.addStringProperty("eventLong");
		events.addStringProperty("eventName");
		events.addStringProperty("eventTime");
		events.setHasKeepSections(true);
	}
}
