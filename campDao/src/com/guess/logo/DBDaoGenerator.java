package com.guess.logo;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class DBDaoGenerator {
	public static void main(String[] args) throws Exception {
		Schema schema = new Schema(1, "com.shadi.dao");
		addEntity("ShadiDetails", schema);
		addEntity("ShadiEvents", schema);
		addEntity("ShadiPhotos", schema);
		ShadiPhotos.addShadiPhotos(schema);
		ShadiDetails.addShadiDetails(schema);
		ShadiEvents.addShadiEvents(schema);
		
//		new DaoGenerator().generateAll(schema, "C:\\Users\\Nitesh Goel\\workspace\\tours\\src");
//		new DaoGenerator().generateAll(schema, "D:\\adt-bundle-windows-x86-20131030\\adt-bundle-windows-x86-20131030\\workspace\\tours\\src");
		new DaoGenerator().generateAll(schema, "/home/kailashkarki/nitin/shadi/campDao/src");
	}
	private static void addEntity(String name, Schema schema) {
		schema.addEntity(name).setTableName(name);
	}

	public static Entity getEntity(String entityName, Schema schema) {
		Entity entity = null;
		for (Entity localEntity : schema.getEntities()) {
			System.out.println("entity.getTableName()--------" + localEntity.getTableName());
			if (localEntity.getTableName() == entityName) {
				entity = localEntity;
				break;
			}
		}
		return entity;
	}
}
