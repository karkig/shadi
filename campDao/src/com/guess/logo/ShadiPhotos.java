package com.guess.logo;

import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class ShadiPhotos {
	public static void addShadiPhotos(Schema schema) {
		Entity room = DBDaoGenerator.getEntity("ShadiPhotos", schema);
	
		room.addStringProperty("id");
		room.addStringProperty("imageId");
		room.addStringProperty("profilePicUrl");
		room.setHasKeepSections(true);
	}
}
