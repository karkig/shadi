package com.guess.logo;

import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class ShadiDetails {
	public static void addShadiDetails(Schema schema) {
		Entity details = DBDaoGenerator.getEntity("ShadiDetails", schema);
		details.addStringProperty("id").primaryKey();
		details.addStringProperty("brideAddress");
		details.addStringProperty("brideAddressLat");
		details.addStringProperty("brideAddressLong");
		details.addStringProperty("brideEmail");
		details.addStringProperty("brideFatherName");
		details.addStringProperty("brideMotherName");
		details.addStringProperty("brideName");
		details.addStringProperty("contactNumber1");
		details.addStringProperty("contactNumber2");
		details.addStringProperty("contactNumber3");
		details.addStringProperty("groomAddress");
		details.addStringProperty("groomAddressLat");
		details.addStringProperty("groomAddressLong");
		details.addStringProperty("groomEmail");
		details.addStringProperty("groomFatherName");
		details.addStringProperty("groomMotherName");
		details.addStringProperty("groomName");
		details.addStringProperty("shadiStory");
		details.setHasKeepSections(true);
	}
}
