package com.google.zxing.client.android;


import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Hashtable;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.android.encode.QRCodeEncoder;
import com.google.zxing.common.BitMatrix;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.Contacts;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class EncoderFunctionality extends Activity{
	
	Button encodeButton;
	EditText encodeText;
	ImageView Imagetoshow;
	QRCodeEncoder qrCodeEncoder ;
	private static final int WHITE = 0xFFFFFFFF;
	  private static final int BLACK = 0xFF000000;
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.encodefunctionality);

        encodeButton = (Button) findViewById(R.id.Encodebutton);
        encodeText = (EditText) findViewById(R.id.Valuestoencode);
        Imagetoshow = (ImageView) findViewById(R.id.Imagetoshow);
        
        encodeButton.setOnClickListener(encodeButtonclicklistener);
        
	}
	
	private OnClickListener encodeButtonclicklistener = new OnClickListener() {
		public void onClick(View v) {
			String valuetoencode = encodeText.getText().toString();
			WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
			Display display = manager.getDefaultDisplay();
			int width = display.getWidth();
			int height = display.getHeight();
			int smallerDimension = width < height ? width : height;
			smallerDimension = smallerDimension * 7 / 8;

			Intent intent = getIntent();
			/*if (qrCodeEncoder == null) { // Odd
				Log.w(TAG, "No existing barcode to send?");
				return true;
			}*/
			if(!(valuetoencode.equalsIgnoreCase("") || valuetoencode.equalsIgnoreCase(null))){
				
				try{
				qrCodeEncoder = new QRCodeEncoder(EncoderFunctionality.this, intent, smallerDimension);
				qrCodeEncoder.encodeQRCodeContents(intent,valuetoencode);
				Bitmap bitmap = /*encodeString(valuetoencode);*/encodeAsBitmap(qrCodeEncoder,valuetoencode);
				Imagetoshow.setImageBitmap(bitmap);
				}
				catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		}
	};
	
	public Bitmap encodeAsBitmap(QRCodeEncoder qrCodeEncoder, String contents) throws WriterException {
	    Hashtable<EncodeHintType,Object> hints = null;
	    WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
		Display display = manager.getDefaultDisplay();
		int width = display.getWidth();
		int height = display.getHeight();
		int smallerDimension = width < height ? width : height;
		smallerDimension = smallerDimension * 7 / 8;
	    String encoding = qrCodeEncoder.guessAppropriateEncoding(contents);
	    if (encoding != null) {
	      hints = new Hashtable<EncodeHintType,Object>(2);
	      hints.put(EncodeHintType.CHARACTER_SET, encoding);
	    }
	    MultiFormatWriter writer = new MultiFormatWriter();
	    BitMatrix result = writer.encode(contents, BarcodeFormat.QR_CODE, smallerDimension, smallerDimension, hints);
	    int width1 = result.getWidth();
	    int height1 = result.getHeight();
	    int[] pixels = new int[width1 * height1];
	    // All are 0, or black, by default
	    for (int y = 0; y < height1; y++) {
	      int offset = y * width1;
	      for (int x = 0; x < width1; x++) {
	        pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
	      }
	    }

	    Bitmap bitmap = Bitmap.createBitmap(width1, height1, Bitmap.Config.ARGB_8888);
	    bitmap.setPixels(pixels, 0, width1, 0, 0, width1, height1);
	    return bitmap;
	  }
	
	 /* public void encodeQRCodeContents(Intent intent, String type) {
		    if (type.equals(Contents.Type.TEXT)) {
		      String data = intent.getStringExtra(Intents.Encode.DATA);
		      if (data != null && data.length() > 0) {
		        contents = data;
		        displayContents = data;
		        title = activity.getString(R.string.contents_text);
		      }
		    } else if (type.equals(Contents.Type.EMAIL)) {
		      String data = trim(intent.getStringExtra(Intents.Encode.DATA));
		      if (data != null) {
		        contents = "mailto:" + data;
		        displayContents = data;
		        title = activity.getString(R.string.contents_email);
		      }
		    } else if (type.equals(Contents.Type.PHONE)) {
		      String data = trim(intent.getStringExtra(Intents.Encode.DATA));
		      if (data != null) {
		        contents = "tel:" + data;
		        displayContents = PhoneNumberUtils.formatNumber(data);
		        title = activity.getString(R.string.contents_phone);
		      }
		    } else if (type.equals(Contents.Type.SMS)) {
		      String data = trim(intent.getStringExtra(Intents.Encode.DATA));
		      if (data != null) {
		        contents = "sms:" + data;
		        displayContents = PhoneNumberUtils.formatNumber(data);
		        title = activity.getString(R.string.contents_sms);
		      }
		    } else if (type.equals(Contents.Type.CONTACT)) {
		      Bundle bundle = intent.getBundleExtra(Intents.Encode.DATA);
		      if (bundle != null) {
		        StringBuilder newContents = new StringBuilder(100);
		        StringBuilder newDisplayContents = new StringBuilder(100);
		        newContents.append("MECARD:");
		        String name = trim(bundle.getString(Contacts.Intents.Insert.NAME));
		        if (name != null) {
		          newContents.append("N:").append(escapeMECARD(name)).append(';');
		          newDisplayContents.append(name);
		        }
		        String address = trim(bundle.getString(Contacts.Intents.Insert.POSTAL));
		        if (address != null) {
		          newContents.append("ADR:").append(escapeMECARD(address)).append(';');
		          newDisplayContents.append('\n').append(address);
		        }
		        for (int x = 0; x < Contents.PHONE_KEYS.length; x++) {
		          String phone = trim(bundle.getString(Contents.PHONE_KEYS[x]));
		          if (phone != null) {
		            newContents.append("TEL:").append(escapeMECARD(phone)).append(';');
		            newDisplayContents.append('\n').append(PhoneNumberUtils.formatNumber(phone));
		          }
		        }
		        for (int x = 0; x < Contents.EMAIL_KEYS.length; x++) {
		          String email = trim(bundle.getString(Contents.EMAIL_KEYS[x]));
		          if (email != null) {
		            newContents.append("EMAIL:").append(escapeMECARD(email)).append(';');
		            newDisplayContents.append('\n').append(email);
		          }
		        }
		        // Make sure we've encoded at least one field.
		        if (newDisplayContents.length() > 0) {
		          newContents.append(';');
		          contents = newContents.toString();
		          displayContents = newDisplayContents.toString();
		          title = activity.getString(R.string.contents_contact);
		        } else {
		          contents = null;
		          displayContents = null;
		        }
		      }
		    } else if (type.equals(Contents.Type.LOCATION)) {
		      Bundle bundle = intent.getBundleExtra(Intents.Encode.DATA);
		      if (bundle != null) {
		        // These must use Bundle.getFloat(), not getDouble(), it's part of the API.
		        float latitude = bundle.getFloat("LAT", Float.MAX_VALUE);
		        float longitude = bundle.getFloat("LONG", Float.MAX_VALUE);
		        if (latitude != Float.MAX_VALUE && longitude != Float.MAX_VALUE) {
		          contents = "geo:" + latitude + ',' + longitude;
		          displayContents = latitude + "," + longitude;
		          title = activity.getString(R.string.contents_location);
		        }
		      }
		    }
		  }*/
	
}
