package com.coverflow;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import at.technikum.mti.fancycoverflow.FancyCoverFlow;
import at.technikum.mti.fancycoverflow.FancyCoverFlowAdapter;

import com.shadi.dao.ShadiDetails;
import com.shadi.dao.ShadiEvents;
import com.shadiconnect.DBUtils;
import com.shadiconnect.DateFormater;
import com.shadiconnect.HomeActivity;
import com.shaadiconnect.*;
import com.shadiconnect.StringUtils;

public class CoverFlowEvent extends Activity {
	static int width ;
	static int height;
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.setContentView(R.layout.layout_inflate_events);
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		 width = size.x;
		 height = size.y;

		try {
			List<ShadiEvents> list = DBUtils
					.getMarriageEvent(StringUtils.Shadi_Id);

			FancyCoverFlow fancyCoverFlow = (FancyCoverFlow) findViewById(R.id.fancyCoverFlow);
			fancyCoverFlow.setReflectionGap(0);
			fancyCoverFlow.setAdapter(new ViewGroupExampleAdapter(list));
			fancyCoverFlow.setSpacing(-50);
			fancyCoverFlow.setUnselectedScale(0.75f);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private static class ViewGroupExampleAdapter extends FancyCoverFlowAdapter {

		List<ShadiEvents> list = null;

		ViewGroupExampleAdapter(List<ShadiEvents> l) {
			list = l;
		}

		@Override
		public int getCount() {
			return list.size();
		}

		@Override
		public Integer getItem(int i) {
			return null;
		}

		@Override
		public long getItemId(int i) {
			return i;
		}

		@Override
		public View getCoverFlowItem(int i, View reuseableView,
				ViewGroup viewGroup) {
			CustomViewGroup customViewGroup = null;

			if (reuseableView != null) {
				customViewGroup = (CustomViewGroup) reuseableView;
			} else {
				customViewGroup = new CustomViewGroup(viewGroup.getContext(),
						i, list);
				
				customViewGroup
						.setLayoutParams(new FancyCoverFlow.LayoutParams(width*2/3,
								(height/2)-20));
			}

			// customViewGroup.getImageView().setImageResource(this.getItem(i));

			return customViewGroup;
		}
	}

	private static class CustomViewGroup extends LinearLayout {

		private ImageView imageView;
		Activity activity;
		TextView event_date, event_time, event_address, event_name;
		int position;

		private CustomViewGroup(Context context, int i, List<ShadiEvents> list) {
			super(context);
			position = i;
			this.activity = (Activity) context;
			// ShadiEvents events = DBUtils.getEventList();
			LayoutInflater inflater = activity.getLayoutInflater();
			View childInflatedView = (View) inflater.inflate(
					R.layout.coverflow_event, null);
			event_name = (TextView) childInflatedView
					.findViewById(R.id.event_name);
			event_date = (TextView) childInflatedView
					.findViewById(R.id.event_date);
			event_time = (TextView) childInflatedView
					.findViewById(R.id.event_time);
			event_address = (TextView) childInflatedView
					.findViewById(R.id.event_address);
			event_name.setText(list.get(i).getEventName() + ",");
			String eventDateTime = list.get(i).getEventTime();
			String eventDate = DateFormater.getDate(eventDateTime);
			event_date.setText(eventDate);
			String eventTime = DateFormater.getTime(eventDateTime);
			event_time.setText(eventTime);
			event_address.setText(list.get(i).getEventAddress());
			this.setOrientation(VERTICAL);
			this.setWeightSum(5);

			LayoutParams layoutParams = new LayoutParams(width*2/3, (height/2)-20);

			childInflatedView.setLayoutParams(layoutParams);

			this.addView(childInflatedView);
		}
	}
}
