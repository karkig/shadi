package com.coverflow;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds.Event;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import at.technikum.mti.fancycoverflow.FancyCoverFlow;
import at.technikum.mti.fancycoverflow.FancyCoverFlowAdapter;

import com.shadi.dao.ShadiDetails;
import com.shadi.dao.ShadiEvents;
import com.shadiconnect.DBUtils;
import com.shadiconnect.DateFormater;
import com.shadiconnect.HomeActivity;
import com.shaadiconnect.*;
import com.shadiconnect.ShadiAdapterData;

public class CoverFlowShadi extends Activity {
	static int width ;
	static int height;
	List<ShadiAdapterData> shadiDetailsList;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.setContentView(R.layout.layout_inflate_shadi);
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		 width = size.x;
		 height = size.y;
		FancyCoverFlow fancyCoverFlow = (FancyCoverFlow) findViewById(R.id.shadiCoverFlow);
		fancyCoverFlow.setReflectionGap(0);
		fancyCoverFlow.setSpacing(-50);
		fancyCoverFlow.setUnselectedScale(0.75f);
		try {
			shadiDetailsList = getShadiList();
			CoverFlowListAdapter cfa = new CoverFlowListAdapter(
					shadiDetailsList);
			fancyCoverFlow.setAdapter(cfa);
		} catch (Exception e) {
			e.printStackTrace();
		}

		fancyCoverFlow.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				ShadiAdapterData data = shadiDetailsList.get(arg2);
				String id = data.getShadiDetails().getId();

				Intent intent = new Intent(getApplicationContext(),
						HomeActivity.class);
				intent.putExtra("Uid", id);
				startActivity(intent);

			}
		});
	}

	private static class CoverFlowListAdapter extends FancyCoverFlowAdapter {
		List<ShadiAdapterData> shadiDetailsList = null;

		CoverFlowListAdapter(List<ShadiAdapterData> sdl) {
			this.shadiDetailsList = sdl;

		}

		@Override
		public int getCount() {
			return shadiDetailsList.size();
		}

		@Override
		public Integer getItem(int i) {
			return null;
		}

		@Override
		public long getItemId(int i) {
			return i;
		}

		@Override
		public View getCoverFlowItem(int i, View reuseableView,
				ViewGroup viewGroup) {
			CustomViewGroup customViewGroup = null;

			if (reuseableView != null) {
				customViewGroup = (CustomViewGroup) reuseableView;
			} else {
				customViewGroup = new CustomViewGroup(viewGroup.getContext(),
						i, shadiDetailsList);
				customViewGroup
				.setLayoutParams(new FancyCoverFlow.LayoutParams(width*2/3,
						(height/2)-20 ));
			}
			return customViewGroup;
		}
	}

	private static class CustomViewGroup extends LinearLayout {

		private ImageView imageView;
		Activity activity;
		TextView event_date, event_time, event_address, event_name;

		private CustomViewGroup(Context context, int position,
				List<ShadiAdapterData> shadiDetailsList) {
			super(context);
			ShadiAdapterData sd = shadiDetailsList.get(position);
			this.activity = (Activity) context;

			int index = 0;
			ShadiDetails shadi = sd.getShadiDetails();
			ArrayList<ShadiEvents> list = sd.getList();

			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getEventName().equals("MARRIGE")) {
					index = i;
					break;
				}
			}

			LayoutInflater inflater = activity.getLayoutInflater();
			View childInflatedView = (View) inflater.inflate(
					R.layout.coverflow_shadi, null);
			event_name = (TextView) childInflatedView
					.findViewById(R.id.event_name);
			event_date = (TextView) childInflatedView
					.findViewById(R.id.event_date);
			
			event_time = (TextView) childInflatedView
					.findViewById(R.id.event_time);
			event_address = (TextView) childInflatedView
					.findViewById(R.id.event_address);
			event_name.setText(shadi.getBrideName()+ "  Weds  " + shadi.getGroomName());
		
			String eventDateTime = list.get(index).getEventTime();
			String eventDate = DateFormater.getDate(eventDateTime);
			event_date.setText(eventDate);
			String eventTime = DateFormater.getTime(eventDateTime);
			event_time.setText(eventTime);
			event_address.setText(list.get(index).getEventAddress());

			this.setOrientation(VERTICAL);
			this.setWeightSum(5);

			LayoutParams layoutParams = new LayoutParams(width*2/3,(height/2)-20 );
			/*
			 * LayoutParams layoutParams = new LayoutParams(
			 * ViewGroup.LayoutParams.FILL_PARENT,
			 * ViewGroup.LayoutParams.FILL_PARENT);
			 */

			childInflatedView.setLayoutParams(layoutParams);

			this.addView(childInflatedView);
		}
	}

	public ArrayList<ShadiAdapterData> getShadiList() {
		ArrayList<ShadiAdapterData> shadiList = new ArrayList<ShadiAdapterData>();
		ArrayList<ShadiDetails> list = (ArrayList<ShadiDetails>) DBUtils
				.getShadiList();
		for (ShadiDetails shadi : list) {
			ArrayList<ShadiEvents> eventList = (ArrayList<ShadiEvents>) DBUtils
					.getMarriageEvent(shadi.getId());
			shadiList.add(new ShadiAdapterData(shadi, eventList));
		}

		return shadiList;
	}
}
