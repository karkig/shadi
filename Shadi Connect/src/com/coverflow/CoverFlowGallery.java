package com.coverflow;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import at.technikum.mti.fancycoverflow.FancyCoverFlow;
import at.technikum.mti.fancycoverflow.FancyCoverFlowAdapter;

import com.google.android.gms.common.data.DataBuffer;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.shadi.dao.ShadiDetails;
import com.shadi.dao.ShadiPhotos;
import com.shadiconnect.DBConnection;
import com.shadiconnect.DBUtils;
import com.shadiconnect.HomeActivity;
import com.shaadiconnect.*;
import com.shadiconnect.ShadiStoryActivity;
import com.shadiconnect.StringUtils;

public class CoverFlowGallery extends Activity {
	List<ShadiPhotos> shadiPhotos;
	Button btn_story;
	static int width;
	static int height;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.setContentView(R.layout.layout_inflate_gallery);
		btn_story = (Button) findViewById(R.id.btn_story);
		btn_story.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent = new Intent(getApplicationContext(),
						ShadiStoryActivity.class);
				startActivity(intent);
			}
		});
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		width = size.x;
		height = size.y;
		FancyCoverFlow fancyCoverFlow = (FancyCoverFlow) findViewById(R.id.fancyCoverFlow);
		fancyCoverFlow.setReflectionGap(0);
		try {
			shadiPhotos = DBUtils.getPhotos(StringUtils.Shadi_Id);
			ShadiDetails sd = DBUtils.getShadiDetails(StringUtils.Shadi_Id);
			if (sd.getShadiStory() != null && !sd.getShadiStory().equals("")) {
				btn_story.setVisibility(View.VISIBLE);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		fancyCoverFlow.setAdapter(new ViewGroupExampleAdapter(shadiPhotos));
		fancyCoverFlow.setSpacing(-50);
		fancyCoverFlow.setUnselectedScale(0.75f);
	}

	private static class ViewGroupExampleAdapter extends FancyCoverFlowAdapter {
		List<ShadiPhotos> shadiPhotos;

		ViewGroupExampleAdapter(List<ShadiPhotos> sp) {
			shadiPhotos = sp;
		}

		@Override
		public int getCount() {
			return shadiPhotos.size();
		}

		@Override
		public Integer getItem(int i) {
			return null;
		}

		@Override
		public long getItemId(int i) {
			return i;
		}

		@Override
		public View getCoverFlowItem(int i, View reuseableView,
				ViewGroup viewGroup) {
			CustomViewGroup customViewGroup = null;

			if (reuseableView != null) {
				customViewGroup = (CustomViewGroup) reuseableView;
			} else {
				customViewGroup = new CustomViewGroup(viewGroup.getContext(),
						i, shadiPhotos);
				customViewGroup
						.setLayoutParams(new FancyCoverFlow.LayoutParams(
								width * 2 / 3, (height / 2) - 20));
			}
			return customViewGroup;
		}
	}

	private static class CustomViewGroup extends LinearLayout {

		private ImageView imageView;
		Activity activity;

		private CustomViewGroup(Context context, int i,
				List<ShadiPhotos> shadiPhotos) {
			super(context);
			this.activity = (Activity) context;
			LayoutInflater inflater = activity.getLayoutInflater();
			View childInflatedView = (View) inflater.inflate(
					R.layout.coverflow_gallery, null);
			imageView = (ImageView) childInflatedView
					.findViewById(R.id.event_brid_groom_img);
			try {
				ImageLoader.getInstance().displayImage(
						shadiPhotos.get(i).getProfilePicUrl(), imageView,
						DBConnection.options);

			} catch (Exception e) {
				e.printStackTrace();
			}
			this.setOrientation(VERTICAL);
			this.setWeightSum(5);
			LayoutParams layoutParams = new LayoutParams(width * 2 / 3,
					(height / 2) - 20);

			childInflatedView.setLayoutParams(layoutParams);

			this.addView(childInflatedView);
		}

	}
}
