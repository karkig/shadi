package com.shadi.network;

import android.content.Context;
import android.net.ConnectivityManager;

public class NetWorkInfomation {
	 public static boolean isOnline(Context ctx){
	        ConnectivityManager cm = (ConnectivityManager) ctx
	                .getSystemService(Context.CONNECTIVITY_SERVICE);

	        boolean ret = cm.getActiveNetworkInfo() != null
	                && cm.getActiveNetworkInfo().isAvailable()
	                && cm.getActiveNetworkInfo().isConnected();
	        return ret;
	    }
	 
	 
}
