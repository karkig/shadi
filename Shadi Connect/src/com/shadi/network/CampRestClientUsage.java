package com.shadi.network;

import org.apache.http.Header;

import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.shadi.gson.ResponseGson;
import com.shadi.listener.ShadiDetailListener;
import com.shadiconnect.DBUtils;

public class CampRestClientUsage {
	public ShadiDetailListener shadiLister;
	

	public void setShadiLister(ShadiDetailListener shadiLister) {
		this.shadiLister = shadiLister;
	}


	public void getShadiDetailFromApi(String url) {
		CampRestClient.get(url, null, null,
				new BaseJsonHttpResponseHandler<ResponseGson>() {

					@Override
					public void onFailure(int arg0, Header[] arg1,
							Throwable arg2, String arg3,
							ResponseGson responseBody) {
						
						shadiLister.dataLoaded(1);
					}

					@Override
					public void onSuccess(int arg0, Header[] arg1, String arg2,
							ResponseGson responseBody) {

						try {
							DBUtils.setResponseGsonToDB(responseBody);
							shadiLister.dataLoaded(0);

						} catch (Exception e) {
							e.printStackTrace();
						}

					}

					@Override
					protected ResponseGson parseResponse(String responseBody,
							boolean arg1) throws Throwable {
						ResponseGson shadiGson;
						
						try {
							Gson gson = new Gson();
							shadiGson = gson.fromJson(responseBody,
									ResponseGson.class);
							
						
							return shadiGson;

						} catch (Exception e) {
							System.out.println(e);
						}

						return null;
					}

				});
	}
}
