package com.shadi.network;

import android.content.Context;
import android.content.SharedPreferences;

import org.apache.http.HttpEntity;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.ResponseHandlerInterface;

public class CampRestClient {

	public static AsyncHttpClient client = new AsyncHttpClient();

	public static void get(String url, String token, RequestParams params,
			AsyncHttpResponseHandler responseHandler) {
		
		client.get(url, params, responseHandler);
	}

	public static void post(String url, String token, RequestParams params,
			AsyncHttpResponseHandler responseHandler) {
		// client.setTimeout(100000);

	

		client.post(url, params, responseHandler);
	}

	public static void post(Context context, String token, String url,
			HttpEntity entity, String contentType,
			ResponseHandlerInterface responseHandler) {
		// client.setTimeout(100000);
	
		client.post(null, url, entity, contentType, responseHandler);
	}
}
