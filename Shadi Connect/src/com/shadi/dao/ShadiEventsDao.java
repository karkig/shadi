package com.shadi.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;

import com.shadi.dao.ShadiEvents;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table ShadiEvents.
*/
public class ShadiEventsDao extends AbstractDao<ShadiEvents, Void> {

    public static final String TABLENAME = "ShadiEvents";

    /**
     * Properties of entity ShadiEvents.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Id = new Property(0, String.class, "id", false, "ID");
        public final static Property EventAddress = new Property(1, String.class, "eventAddress", false, "EVENT_ADDRESS");
        public final static Property EventDate = new Property(2, String.class, "eventDate", false, "EVENT_DATE");
        public final static Property EventLat = new Property(3, String.class, "eventLat", false, "EVENT_LAT");
        public final static Property EventLong = new Property(4, String.class, "eventLong", false, "EVENT_LONG");
        public final static Property EventName = new Property(5, String.class, "eventName", false, "EVENT_NAME");
        public final static Property EventTime = new Property(6, String.class, "eventTime", false, "EVENT_TIME");
    };


    public ShadiEventsDao(DaoConfig config) {
        super(config);
    }
    
    public ShadiEventsDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "'ShadiEvents' (" + //
                "'ID' TEXT," + // 0: id
                "'EVENT_ADDRESS' TEXT," + // 1: eventAddress
                "'EVENT_DATE' TEXT," + // 2: eventDate
                "'EVENT_LAT' TEXT," + // 3: eventLat
                "'EVENT_LONG' TEXT," + // 4: eventLong
                "'EVENT_NAME' TEXT," + // 5: eventName
                "'EVENT_TIME' TEXT);"); // 6: eventTime
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "'ShadiEvents'";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, ShadiEvents entity) {
        stmt.clearBindings();
 
        String id = entity.getId();
        if (id != null) {
            stmt.bindString(1, id);
        }
 
        String eventAddress = entity.getEventAddress();
        if (eventAddress != null) {
            stmt.bindString(2, eventAddress);
        }
 
        String eventDate = entity.getEventDate();
        if (eventDate != null) {
            stmt.bindString(3, eventDate);
        }
 
        String eventLat = entity.getEventLat();
        if (eventLat != null) {
            stmt.bindString(4, eventLat);
        }
 
        String eventLong = entity.getEventLong();
        if (eventLong != null) {
            stmt.bindString(5, eventLong);
        }
 
        String eventName = entity.getEventName();
        if (eventName != null) {
            stmt.bindString(6, eventName);
        }
 
        String eventTime = entity.getEventTime();
        if (eventTime != null) {
            stmt.bindString(7, eventTime);
        }
    }

    /** @inheritdoc */
    @Override
    public Void readKey(Cursor cursor, int offset) {
        return null;
    }    

    /** @inheritdoc */
    @Override
    public ShadiEvents readEntity(Cursor cursor, int offset) {
        ShadiEvents entity = new ShadiEvents( //
            cursor.isNull(offset + 0) ? null : cursor.getString(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1), // eventAddress
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // eventDate
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // eventLat
            cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4), // eventLong
            cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5), // eventName
            cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6) // eventTime
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, ShadiEvents entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getString(offset + 0));
        entity.setEventAddress(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setEventDate(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setEventLat(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setEventLong(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
        entity.setEventName(cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5));
        entity.setEventTime(cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6));
     }
    
    /** @inheritdoc */
    @Override
    protected Void updateKeyAfterInsert(ShadiEvents entity, long rowId) {
        // Unsupported or missing PK type
        return null;
    }
    
    /** @inheritdoc */
    @Override
    public Void getKey(ShadiEvents entity) {
        return null;
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
}
