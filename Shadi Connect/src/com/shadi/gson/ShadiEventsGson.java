package com.shadi.gson;


public class ShadiEventsGson {
	private String EventAddress;
    private String EventDate;
    private String EventLatitude;
    private String EventLongitude;
    private String EventName;
    private String EventTime;

    public String getEventAddress() {
		return EventAddress;
	}
	public void setEventAddress(String eventAddress) {
		EventAddress = eventAddress;
	}
	public String getEventDate() {
		return EventDate;
	}
	public void setEventDate(String eventDate) {
		EventDate = eventDate;
	}
	public String getEventLatitude() {
		return EventLatitude;
	}
	public void setEventLatitude(String eventLatitude) {
		EventLatitude = eventLatitude;
	}
	public String getEventLongitude() {
		return EventLongitude;
	}
	public void setEventLongitude(String eventLongitude) {
		EventLongitude = eventLongitude;
	}
	public String getEventName() {
		return EventName;
	}
	public void setEventName(String eventName) {
		EventName = eventName;
	}
	public String getEventTime() {
		return EventTime;
	}
	public void setEventTime(String eventTime) {
		EventTime = eventTime;
	}
	

}
