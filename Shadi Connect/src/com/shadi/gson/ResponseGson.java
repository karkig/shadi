package com.shadi.gson;

import java.util.ArrayList;

import com.shadi.gson.ShadiDetailsGson;
import com.shadi.gson.ShadiEventsGson;
import com.shadi.gson.ShadiPhotosGson;

public class ResponseGson {
	
	public boolean Success;
	public ShadiDetailsGson ShaadiDetails;
	public ArrayList<ShadiEventsGson>  ShaadiEvents;
	public ArrayList<ShadiPhotosGson>  ShaadiPhotos;
	
	public String errorMessage = null;
	public String tag = null;
	
	public ShadiDetailsGson getShaadiDetails() {
		return ShaadiDetails;
	}
	public void setShaadiDetails(ShadiDetailsGson shaadiDetails) {
		ShaadiDetails = shaadiDetails;
	}
	public ArrayList<ShadiEventsGson> getShaadiEvents() {
		return ShaadiEvents;
	}
	public void setShaadiEvents(ArrayList<ShadiEventsGson> shaadiEvents) {
		ShaadiEvents = shaadiEvents;
	}
	public ArrayList<ShadiPhotosGson> getShaadiPhotos() {
		return ShaadiPhotos;
	}
	public void setShaadiPhotos(ArrayList<ShadiPhotosGson> shaadiPhotos) {
		ShaadiPhotos = shaadiPhotos;
	}
	
}
