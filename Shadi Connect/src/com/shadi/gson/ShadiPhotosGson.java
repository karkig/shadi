package com.shadi.gson;

public class ShadiPhotosGson {
	private String Id;
    private String ImageUUID;
    private String ProfilePicUrl;
    public String getId() {
		return Id;
	}
	public void setId(String id) {
		Id = id;
	}
	public String getImageUUID() {
		return ImageUUID;
	}
	public void setImageUUID(String imageUUID) {
		ImageUUID = imageUUID;
	}
	public String getProfilePicUrl() {
		return ProfilePicUrl;
	}
	public void setProfilePicUrl(String profilePicUrl) {
		ProfilePicUrl = profilePicUrl;
	}


}
