package com.shadi.gson;

public class ShadiDetailsGson {
	private String BrideName;
    private String BrideFatherName;
    private String BrideMotherName;
    private String BrideAddress;
    private String BrideEmail;
    private String BrideAddressLatitude;
    private String BrideAddressLongitude;
    private String GroomName;
    private String GroomFatherName;
    private String GroomMotherName;
    private String GroomAddress;
    private String GroomEmail;
    private String GroomAddressLatitude;
    private String GroomAddressLongitude;
    private String ContactNumber1;
    private String ContactNumber2;
    private String ContactNumber3;
    private String ShadiStory;
	
    public String getBrideName() {
		return BrideName;
	}
	public void setBrideName(String brideName) {
		BrideName = brideName;
	}
	public String getBrideFatherName() {
		return BrideFatherName;
	}
	public void setBrideFatherName(String brideFatherName) {
		BrideFatherName = brideFatherName;
	}
	public String getBrideMotherName() {
		return BrideMotherName;
	}
	public void setBrideMotherName(String brideMotherName) {
		BrideMotherName = brideMotherName;
	}
	public String getBrideAddress() {
		return BrideAddress;
	}
	public void setBrideAddress(String brideAddress) {
		BrideAddress = brideAddress;
	}
	public String getBrideEmail() {
		return BrideEmail;
	}
	public void setBrideEmail(String brideEmail) {
		BrideEmail = brideEmail;
	}
	public String getBrideAddressLatitude() {
		return BrideAddressLatitude;
	}
	public void setBrideAddressLatitude(String brideAddressLatitude) {
		BrideAddressLatitude = brideAddressLatitude;
	}
	public String getBrideAddressLongitude() {
		return BrideAddressLongitude;
	}
	public void setBrideAddressLongitude(String brideAddressLongitude) {
		BrideAddressLongitude = brideAddressLongitude;
	}
	public String getGroomName() {
		return GroomName;
	}
	public void setGroomName(String groomName) {
		GroomName = groomName;
	}
	public String getGroomFatherName() {
		return GroomFatherName;
	}
	public void setGroomFatherName(String groomFatherName) {
		GroomFatherName = groomFatherName;
	}
	public String getGroomMotherName() {
		return GroomMotherName;
	}
	public void setGroomMotherName(String groomMotherName) {
		GroomMotherName = groomMotherName;
	}
	public String getGroomAddress() {
		return GroomAddress;
	}
	public void setGroomAddress(String groomAddress) {
		GroomAddress = groomAddress;
	}
	public String getGroomEmail() {
		return GroomEmail;
	}
	public void setGroomEmail(String groomEmail) {
		GroomEmail = groomEmail;
	}
	public String getGroomAddressLatitude() {
		return GroomAddressLatitude;
	}
	public void setGroomAddressLatitude(String groomAddressLatitude) {
		GroomAddressLatitude = groomAddressLatitude;
	}
	public String getGroomAddressLongitude() {
		return GroomAddressLongitude;
	}
	public void setGroomAddressLongitude(String groomAddressLongitude) {
		GroomAddressLongitude = groomAddressLongitude;
	}
	public String getContactNumber1() {
		return ContactNumber1;
	}
	public void setContactNumber1(String contactNumber1) {
		ContactNumber1 = contactNumber1;
	}
	public String getContactNumber2() {
		return ContactNumber2;
	}
	public void setContactNumber2(String contactNumber2) {
		ContactNumber2 = contactNumber2;
	}
	public String getContactNumber3() {
		return ContactNumber3;
	}
	public void setContactNumber3(String contactNumber3) {
		ContactNumber3 = contactNumber3;
	}
	public String getShadiStory() {
		return ShadiStory;
	}
	public void setShadiStory(String shadiStory) {
		ShadiStory = shadiStory;
	}
	


}
