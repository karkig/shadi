package com.shadiconnect;

import java.util.ArrayList;
import java.util.List;

import com.shadi.dao.DaoSession;
import com.shadi.dao.ShadiDetails;
import com.shadi.dao.ShadiDetailsDao;
import com.shadi.dao.ShadiEvents;
import com.shadi.dao.ShadiEventsDao;
import com.shadi.dao.ShadiPhotos;
import com.shadi.dao.ShadiPhotosDao;
import com.shadi.gson.ResponseGson;
import com.shadi.gson.ShadiDetailsGson;
import com.shadi.gson.ShadiEventsGson;
import com.shadi.gson.ShadiPhotosGson;

/**
 */
public class DBUtils {
	public static DaoSession daoSession = DBConnection.getDaoSession();
	private static ShadiDetailsDao shadiDetailsDao = daoSession
			.getShadiDetailsDao();
	private static ShadiEventsDao shadiEventsDao = daoSession
			.getShadiEventsDao();
	private static ShadiPhotosDao shadiPhotosDao = daoSession
			.getShadiPhotosDao();

	public static void setResponseGsonToDB(ResponseGson responseBody) {

		ShadiDetailsGson shadiDetailsGson = responseBody.getShaadiDetails();
		List<ShadiEventsGson> shadiEventsGson = responseBody.getShaadiEvents();
		List<ShadiPhotosGson> shadiPhotosGson = responseBody.getShaadiPhotos();

		setShadiDetailsToDB(shadiDetailsGson);
		setShadiEventsToDB(shadiEventsGson);
		setShadiPhotosToDB(shadiPhotosGson);
	}

	public static void setShadiDetailsToDB(ShadiDetailsGson shadiDetailsGson) {
		ShadiDetails shadiDetail = new ShadiDetails();
		try {

			shadiDetail.setId(StringUtils.UID);
			shadiDetail.setBrideAddress(shadiDetailsGson.getBrideAddress());
			shadiDetail.setBrideAddressLat(shadiDetailsGson
					.getBrideAddressLatitude());
			shadiDetail.setBrideAddressLong(shadiDetailsGson
					.getBrideAddressLongitude());
			shadiDetail.setBrideEmail(shadiDetailsGson.getBrideEmail());
			shadiDetail.setBrideFatherName(shadiDetailsGson
					.getBrideFatherName());
			shadiDetail.setBrideMotherName(shadiDetailsGson
					.getBrideMotherName());
			shadiDetail.setBrideName(shadiDetailsGson.getBrideName());
			shadiDetail.setContactNumber1(shadiDetailsGson.getContactNumber1());
			shadiDetail.setContactNumber2(shadiDetailsGson.getContactNumber2());
			shadiDetail.setContactNumber3(shadiDetailsGson.getContactNumber3());
			shadiDetail.setGroomAddress(shadiDetailsGson.getGroomAddress());
			shadiDetail.setGroomAddressLat(shadiDetailsGson
					.getGroomAddressLatitude());
			shadiDetail.setGroomAddressLong(shadiDetailsGson
					.getGroomAddressLongitude());
			shadiDetail.setGroomEmail(shadiDetailsGson.getGroomEmail());
			shadiDetail.setGroomFatherName(shadiDetailsGson
					.getGroomFatherName());
			shadiDetail.setGroomMotherName(shadiDetailsGson
					.getGroomMotherName());
			shadiDetail.setGroomName(shadiDetailsGson.getGroomName());
			shadiDetail.setShadiStory(shadiDetailsGson.getShadiStory());
			shadiDetailsDao.insertOrReplaceInTx(shadiDetail);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void setShadiEventsToDB(List<ShadiEventsGson> shadiEventsGson) {
		try {
			List<ShadiEvents> eventList = new ArrayList<ShadiEvents>();
			ShadiEvents shadiEvent;
			for (ShadiEventsGson seg : shadiEventsGson) {
				shadiEvent = new ShadiEvents();
				shadiEvent.setId(StringUtils.UID);
				shadiEvent.setEventAddress(seg.getEventAddress());
				shadiEvent.setEventDate(seg.getEventDate());
				shadiEvent.setEventLat(seg.getEventLatitude());
				shadiEvent.setEventLong(seg.getEventLongitude());
				shadiEvent.setEventName(seg.getEventName());
				shadiEvent.setEventTime(seg.getEventTime());
				eventList.add(shadiEvent);
			}
			shadiEventsDao.insertOrReplaceInTx(eventList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void setShadiPhotosToDB(List<ShadiPhotosGson> shadiPhotosGson) {
		try {
			List<ShadiPhotos> photoList = new ArrayList<ShadiPhotos>();
			ShadiPhotos shadiPhoto;
			for (ShadiPhotosGson spg : shadiPhotosGson) {
				shadiPhoto = new ShadiPhotos();
				shadiPhoto.setId(StringUtils.UID);
				shadiPhoto.setImageId(spg.getImageUUID());
				shadiPhoto.setProfilePicUrl(spg.getProfilePicUrl());
				photoList.add(shadiPhoto);
			}
			shadiPhotosDao.insertOrReplaceInTx(photoList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static 	List<ShadiEvents> getEventList() {
		List<ShadiEvents> events = null;
		try {
			events = shadiEventsDao.queryBuilder().where(ShadiDetailsDao.Properties.Id.eq(StringUtils.UID)).list();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return events;
	}
	
public static ShadiDetails getShadiDetails(String uid)
{
	ShadiDetails shadiDetails = null ;
	try {
		shadiDetails = shadiDetailsDao.queryBuilder().where(ShadiDetailsDao.Properties.Id.eq(uid)).unique();
	} catch (Exception e) {
		e.printStackTrace();
	}

	return shadiDetails;
	
}
	public static List<ShadiDetails> getShadiList() {
		List<ShadiDetails> shadiDetails = null;
		try {
			shadiDetails = shadiDetailsDao.queryBuilder().list();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return shadiDetails;

	}

	public static List<ShadiPhotos> getPhotos(String uid) {
		List<ShadiPhotos> shadiPhotos = null;
		try {
			shadiPhotos = shadiPhotosDao.queryBuilder().where(ShadiPhotosDao.Properties.Id.eq(uid)).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return shadiPhotos;
	}

	public static List<ShadiEvents> getMarriageEvent(String uid) {
		
		List<ShadiEvents> events = null;
		try {
			events = shadiEventsDao.queryBuilder().where(ShadiEventsDao.Properties.Id.eq(uid)).list();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return events;		
	}

	public static List<ShadiPhotos> getShadiPhotos(String uid) {
		// TODO Auto-generated method stub
		List<ShadiPhotos> photos = null;
		try {
			photos = shadiPhotosDao.queryBuilder().where(ShadiPhotosDao.Properties.Id.eq(uid)).list();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return photos;	
	}
}
