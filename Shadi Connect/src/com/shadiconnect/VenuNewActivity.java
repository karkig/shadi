package com.shadiconnect;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;

import com.google.android.gms.internal.fi;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.shadi.dao.ShadiDetails;
import com.shadi.dao.ShadiEvents;
import com.shadi.network.NetWorkInfomation;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.shaadiconnect.*;
public class VenuNewActivity extends android.support.v4.app.FragmentActivity {
	GoogleMap map;
	SupportMapFragment fm;
	private LatLng BRIDE_LAT_LNG = null;
	private LatLng GROOM_LAT_LNG = null;
	private LatLng VENUE_LAT_LNG = null;
	private LatLng CURRENT_LAT_LNG = null;
	private boolean shouldFocus = true;
	Button groom, bride, venue;
	public ProgressDialog progressDialog;
	ArrayList<LatLng> markerPoints;
	String groomAddress,brideAddress,venuAddress;
	Button map_refresh ;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_venue);
		 progressDialog = new ProgressDialog(VenuNewActivity.this);
         progressDialog.setMessage("Please Wait...");
         progressDialog.setCanceledOnTouchOutside(false);
         progressDialog.setCancelable(false);
		groom = (Button) findViewById(R.id.groom);
		bride = (Button) findViewById(R.id.bride);
		venue = (Button) findViewById(R.id.venue);
		map_refresh = (Button) findViewById(R.id.map_refresh);
		map_refresh.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				progressDialog.show();
				map.clear();
				setMarker();
				CameraUpdate center = CameraUpdateFactory
						.newLatLng(CURRENT_LAT_LNG);
				CameraUpdate zoom = CameraUpdateFactory.zoomTo(10);
				
				map.moveCamera(center);
				map.animateCamera(zoom);
				progressDialog.hide();
			}
		});

		groom.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				progressDialog.show();
				map.clear();

				try {
					String url = getDirectionsUrl(GROOM_LAT_LNG,
							CURRENT_LAT_LNG);
					CameraUpdate center = CameraUpdateFactory
							.newLatLng(CURRENT_LAT_LNG);
					CameraUpdate zoom = CameraUpdateFactory.zoomTo(10);
					
					map.moveCamera(center);
					map.animateCamera(zoom);
					DownloadTask downloadTask = new DownloadTask();
					downloadTask.execute(url);
					setMarker();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
		bride.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				progressDialog.show();
				map.clear();

				try {
					String url = getDirectionsUrl(BRIDE_LAT_LNG,
							CURRENT_LAT_LNG);
					CameraUpdate center = CameraUpdateFactory
							.newLatLng(CURRENT_LAT_LNG);
					CameraUpdate zoom = CameraUpdateFactory.zoomTo(10);
					
					map.moveCamera(center);
					map.animateCamera(zoom);
					DownloadTask downloadTask = new DownloadTask();
					downloadTask.execute(url);
					setMarker();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

		venue.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				progressDialog.show();
				map.clear();

				try {
					String url = getDirectionsUrl(VENUE_LAT_LNG,
							CURRENT_LAT_LNG);
					CameraUpdate center = CameraUpdateFactory
							.newLatLng(CURRENT_LAT_LNG);
					CameraUpdate zoom = CameraUpdateFactory.zoomTo(10);
					
					map.moveCamera(center);
					map.animateCamera(zoom);
					DownloadTask downloadTask = new DownloadTask();
					downloadTask.execute(url);
					setMarker();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

		ShadiDetails shadiDetails = DBUtils.getShadiDetails(StringUtils.Shadi_Id);
		brideAddress = shadiDetails.getBrideAddress();
		groomAddress = shadiDetails.getGroomAddress();
		fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(
				R.id.map);
		map = fm.getMap();

		BRIDE_LAT_LNG = new LatLng(Double.parseDouble(shadiDetails
				.getBrideAddressLat()), Double.parseDouble(shadiDetails
				.getBrideAddressLong()));
		GROOM_LAT_LNG = new LatLng(Double.parseDouble(shadiDetails
				.getGroomAddressLat()), Double.parseDouble(shadiDetails
				.getGroomAddressLong()));

		List<ShadiEvents> list = DBUtils.getMarriageEvent(StringUtils.Shadi_Id);

		for (ShadiEvents se : list) {

			if (!se.getEventName().equals("")) {
				VENUE_LAT_LNG = new LatLng(
						Double.parseDouble(se.getEventLat()),
						Double.parseDouble(se.getEventLong()));
				venuAddress = se.getEventAddress();
				break;
			}
		}

		if (map != null) {

			try {
				map.setInfoWindowAdapter(new InfoWindowAdapter() {
			        @Override
			        public View getInfoWindow(Marker arg0) {
			            return null;
			        }
			        @Override
			        public View getInfoContents(Marker marker) {
			            View myContentView = getLayoutInflater().inflate(
			                    R.layout.map_marker_window, null);
			            ImageView img = (ImageView) myContentView.findViewById(R.id.map_marker_win_img);
			            TextView tvTitle = ((TextView) myContentView
			                    .findViewById(R.id.map_marker_win_title));
			            tvTitle.setText(marker.getTitle());
			           String str = marker.getTitle();
			           if(str.equals(brideAddress))
			           {
			        	   
			        	   Resources res = getResources(); // need this to fetch the drawable
			        	   Drawable draw = res.getDrawable( R.drawable.map_bride_nav_icon );
			        	   img.setImageDrawable(draw);
			           }
			           else if(str.equals(groomAddress))
			           {
			        	   Resources res = getResources(); // need this to fetch the drawable
			        	   Drawable draw = res.getDrawable( R.drawable.map_groom_nav_icon );
			        	   img.setImageDrawable(draw);
			           }
			           else  if(str.equals(venuAddress))
			           {
			        	   Resources res = getResources(); // need this to fetch the drawable
			        	   Drawable draw = res.getDrawable( R.drawable.map_event_nav_icon );
			        	   img.setImageDrawable(draw);
			           }
			           
			            return myContentView;
			        }
			    });

				map.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {

					@Override
					public void onMyLocationChange(Location arg0) {

						CURRENT_LAT_LNG = new LatLng(arg0.getLatitude(), arg0
								.getLongitude());
						if (shouldFocus) {
							shouldFocus = false;

							CameraUpdate center = CameraUpdateFactory
									.newLatLng(CURRENT_LAT_LNG);
							CameraUpdate zoom = CameraUpdateFactory.zoomTo(10);
							map.addMarker(new MarkerOptions().position(CURRENT_LAT_LNG).title(
									"Your are here ").icon(BitmapDescriptorFactory
											.fromResource(R.drawable.map_icon_show)));
							map.moveCamera(center);
							map.animateCamera(zoom);
						}

					}
				});
				setMarker();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void setMarker() {
		map.setMyLocationEnabled(true);
		map.addMarker(new MarkerOptions()
				.position(BRIDE_LAT_LNG)
				.title(brideAddress)
				.icon(BitmapDescriptorFactory
						.fromResource(R.drawable.map_icon_show)));

		map.addMarker(new MarkerOptions()
				.position(GROOM_LAT_LNG)
				.title(groomAddress)
				.icon(BitmapDescriptorFactory
						.fromResource(R.drawable.map_icon_show)));

		map.addMarker(new MarkerOptions()
				.position(VENUE_LAT_LNG)
				.title(venuAddress)
				.icon(BitmapDescriptorFactory.fromResource(R.drawable.map_icon_show)));

		map.addMarker(new MarkerOptions().position(CURRENT_LAT_LNG).title(
				"Your are here "));

	}

	private String getDirectionsUrl(LatLng origin, LatLng dest) {

		String str_origin = "origin=" + origin.latitude + ","
				+ origin.longitude;

		String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

		String sensor = "sensor=false";

		String parameters = str_origin + "&" + str_dest + "&" + sensor;

		String output = "json";

		String url = "https://maps.googleapis.com/maps/api/directions/"
				+ output + "?" + parameters;

		return url;
	}

	/** A method to download json data from url */
	private String downloadUrl(String strUrl) throws IOException {
		String data = "";
		InputStream iStream = null;
		HttpURLConnection urlConnection = null;
		try {
			URL url = new URL(strUrl);

			// Creating an http connection to communicate with url
			urlConnection = (HttpURLConnection) url.openConnection();

			// Connecting to url
			urlConnection.connect();

			// Reading data from url
			iStream = urlConnection.getInputStream();

			BufferedReader br = new BufferedReader(new InputStreamReader(
					iStream));

			StringBuffer sb = new StringBuffer();

			String line = "";
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

			data = sb.toString();

			br.close();

		} catch (Exception e) {
		} finally {
			iStream.close();
			urlConnection.disconnect();
		}
		return data;
	}

	// Fetches data from url passed
	private class DownloadTask extends AsyncTask<String, Void, String> {

		// Downloading data in non-ui thread
		@Override
		protected String doInBackground(String... url) {

			// For storing data from web service
			String data = "";

			try {
				// Fetching the data from web service
				data = downloadUrl(url[0]);
			} catch (Exception e) {
			}
			return data;
		}

		// Executes in UI thread, after the execution of
		// doInBackground()
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			ParserTask parserTask = new ParserTask();

			// Invokes the thread for parsing the JSON data
			parserTask.execute(result);
		}
	}

	/** A class to parse the Google Places in JSON format */
	private class ParserTask extends
			AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

		// Parsing the data in non-ui thread
		@Override
		protected List<List<HashMap<String, String>>> doInBackground(
				String... jsonData) {

			JSONObject jObject;
			List<List<HashMap<String, String>>> routes = null;

			try {
				jObject = new JSONObject(jsonData[0]);
				DirectionsJSONParser parser = new DirectionsJSONParser();

				// Starts parsing data
				routes = parser.parse(jObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return routes;
		}

		// Executes in UI thread, after the parsing process
		@Override
		protected void onPostExecute(List<List<HashMap<String, String>>> result) {
			try{
				ArrayList<LatLng> points = null;
				PolylineOptions lineOptions = null;
				MarkerOptions markerOptions = new MarkerOptions();

				// Traversing through all the routes
				for (int i = 0; i < result.size(); i++) {
					points = new ArrayList<LatLng>();
					lineOptions = new PolylineOptions();

					// Fetching i-th route
					List<HashMap<String, String>> path = result.get(i);

					// Fetching all the points in i-th route
					for (int j = 0; j < path.size(); j++) {
						HashMap<String, String> point = path.get(j);

						double lat = Double.parseDouble(point.get("lat"));
						double lng = Double.parseDouble(point.get("lng"));
						LatLng position = new LatLng(lat, lng);
						points.add(position);
					}

					// Adding all the points in the route to LineOptions
					lineOptions.addAll(points);
					lineOptions.width(5);
					lineOptions.color(Color.RED);
					
				}

				// Drawing polyline in the Google Map for the i-th route
				map.addPolyline(lineOptions);
				progressDialog.hide();
			}catch(Exception e)
			{
				progressDialog.hide();
				 if(!NetWorkInfomation.isOnline(getApplicationContext())){
					 Toast.makeText(getApplicationContext(), "Internet is not available. Check the connection and try again.", Toast.LENGTH_SHORT).show();
		                return;
		            }
				e.printStackTrace();
			}
			
		}
	}
}
