package com.shadiconnect;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import com.shadi.dao.ShadiDetails;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.shaadiconnect.*;

public class ContactActivity extends Activity {
	private Button callBtn;
	ListView contatcListview;
	List<ContactData> data;
	CampAdapter adapter;
	Button msg_btn, whatsapp_btn, mail_button;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_contact);

		init();

	}

	public void init() {
		msg_btn = (Button) findViewById(R.id.msg_btn);
		whatsapp_btn = (Button) findViewById(R.id.whatsapp_btn);
		mail_button = (Button) findViewById(R.id.mail_btn);

		msg_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("sms:"
						+ "")));
			}
		});
		mail_button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_SEND);
				i.setType("message/rfc822");
				i.putExtra(Intent.EXTRA_EMAIL, new String[] { "" });
				i.putExtra(Intent.EXTRA_SUBJECT, "");
				i.putExtra(Intent.EXTRA_TEXT, "");
				try {
					startActivity(Intent.createChooser(i, "Send mail..."));
				} catch (android.content.ActivityNotFoundException ex) {
					// Toast.makeText(this,
					// "There are no email clients installed.",
					// Toast.LENGTH_SHORT).show();
				}
			}
		});
		whatsapp_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_MAIN);
                PackageManager managerclock = getPackageManager();
                i = managerclock.getLaunchIntentForPackage("com.whatsapp");
                i.addCategory(Intent.CATEGORY_LAUNCHER);
                startActivity(i);
			}
		});

		// DBUtils.getData();
		ShadiDetails shadiDetails = DBUtils
				.getShadiDetails(StringUtils.Shadi_Id);

		shadiDetails.getContactNumber1();

		contatcListview = (ListView) findViewById(R.id.listview);

		data = new ArrayList<ContactData>();
		ContactData makeContact;

		if (shadiDetails.getContactNumber1() != null) {
			makeContact = new ContactData(shadiDetails.getContactNumber1());
			data.add(makeContact);
		}

		if (shadiDetails.getContactNumber2() != null) {
			makeContact = new ContactData(shadiDetails.getContactNumber2());
			data.add(makeContact);

		}

		if (shadiDetails.getContactNumber3() != null) {
			makeContact = new ContactData(shadiDetails.getContactNumber3());
			data.add(makeContact);

		}

		adapter = new CampAdapter(ContactActivity.this, R.layout.contact_item,
				data);
		contatcListview.setAdapter(adapter);
		contatcListview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Toast.makeText(getApplicationContext(), "ad",
						Toast.LENGTH_SHORT).show();

			}
		});

	}
}
