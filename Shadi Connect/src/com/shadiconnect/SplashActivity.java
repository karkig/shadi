package com.shadiconnect;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Window;
import com.shaadiconnect.*;
import com.shaadiconnect.*;


public class SplashActivity extends Activity{
	private MyCounter mCounter;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_splash);
		   
		  mCounter = new MyCounter(1000, 1000);
			mCounter.start();
			
}
	private class MyCounter extends CountDownTimer {

		public MyCounter(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onFinish() {
			// TODO Auto-generated method stub
			Intent intent=new Intent(SplashActivity.this,MainActivity.class);
			startActivity(intent);
			finish();
			
		}

		@Override
		public void onTick(long millisUntilFinished) {
			// TODO Auto-generated method stub
		
		}

	}

	
}
