package com.shadiconnect;

import java.io.FileNotFoundException;
import java.util.List;
import com.coverflow.CoverFlowShadi;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Reader;
import com.google.zxing.client.android.CaptureActivity;
import com.google.zxing.common.HybridBinarizer;
import com.shadi.dao.ShadiDetails;
import com.shadi.listener.ShadiDetailListener;
import com.shadi.network.CampRestClientUsage;
import com.shadi.network.NetWorkInfomation;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.Toast;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.shaadiconnect.*;

public class MainActivity extends Activity implements OnClickListener,
		ShadiDetailListener {
	private Button mViewScan;
	private Button crousalBtn;
	private Button gallery;
	public ProgressDialog progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		progressDialog = new ProgressDialog(MainActivity.this);
		progressDialog.setMessage("Please Wait...");
		progressDialog.setCanceledOnTouchOutside(false);
		progressDialog.setCancelable(false);

		mViewScan = (Button) findViewById(R.id.btn_scan);
		mViewScan.setOnClickListener(this);
		crousalBtn = (Button) findViewById(R.id.btn_saveCards);
		gallery = (Button) findViewById(R.id.btn_scanGallery);
		crousalBtn.setOnClickListener(this);
		gallery.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		Intent intent;
		final Context context = this;
		switch (v.getId()) {
		case R.id.btn_scan:
			try
			{
				intent = new Intent(MainActivity.this, CaptureActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
				startActivityForResult(intent, 1);
			}catch (Exception e)
			{
				e.printStackTrace();
			}
			
			break;
		case R.id.btn_saveCards:
			List<ShadiDetails> list = DBUtils.getShadiList();
			if (list.size() <= 0) {
				Toast.makeText(this, "Please Scan a QR Code", Toast.LENGTH_LONG)
						.show();
			} else {
				intent = new Intent(context, CoverFlowShadi.class);
				startActivity(intent);
			}

			break;
		case R.id.btn_scanGallery:
			intent = new Intent(
					Intent.ACTION_PICK,
					android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
			startActivityForResult(intent, 0);
			break;
		}
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == 1) {
			String url = data.getExtras().getString("OR_URL");
			saveQRCodetoPref(url);
			System.out.println("<<<url>>" + url);
		}
		/* Reading QR code and decoding it for corresponding details */
		if (requestCode == 0) {
			if (resultCode == RESULT_OK) {
				Uri targetUri = data.getData();
				Bitmap bitmap;
				try {
					bitmap = BitmapFactory.decodeStream(getContentResolver()
							.openInputStream(targetUri));
					LuminanceSource source = new RGBLuminanceSource(bitmap);
					BinaryBitmap bBitmap = new BinaryBitmap(
							new HybridBinarizer(source));

					Reader reader = new MultiFormatReader();
					com.google.zxing.Result result;

					result = reader.decode(bBitmap);

					saveQRCodetoPref(result.getText());

				} catch (FileNotFoundException e) {
					Toast.makeText(this, " QR code not selected ",
							Toast.LENGTH_LONG).show();
					e.printStackTrace();
				} catch (NotFoundException e) {
					Toast.makeText(this, " QR code not found ",
							Toast.LENGTH_LONG).show();
					e.printStackTrace();
				} catch (ChecksumException e) {
					Toast.makeText(this, " QR code corrupt ", Toast.LENGTH_LONG)
							.show();
					e.printStackTrace();
				} catch (FormatException e) {
					Toast.makeText(this, " QR code corrupt ", Toast.LENGTH_LONG)
							.show();
					e.printStackTrace();
				}
			}
		}
	}

	public void saveQRCodetoPref(String api) {
		StringUtils.API = api;
		if(api.contains("shaadiConnectId"))
		{
			String tokens[] = api.split("shaadiConnectId=");

			if (!tokens[1].equals("")) {
				StringUtils.UID = tokens[1];
				ShadiDetails shadiDetails = DBUtils
						.getShadiDetails(StringUtils.UID);
				if (shadiDetails == null) {
					CampRestClientUsage rest = new CampRestClientUsage();
					rest.setShadiLister(this);
					if (!NetWorkInfomation.isOnline(getApplicationContext())) {
						Toast.makeText(
								getApplicationContext(),
								"Internet is not available. Check the connection and try again.",
								Toast.LENGTH_SHORT).show();
						return;
					}
					progressDialog.show();
					rest.getShadiDetailFromApi(StringUtils.API);
				} else {
					Toast.makeText(this, " Shadi already added ",
							Toast.LENGTH_SHORT).show();
				}

			}
		}else
		{
			Toast.makeText(this,"QR code corrupt", Toast.LENGTH_LONG).show();
		}
		
	}

	@Override
	public void dataLoaded(int statusCode) {

		if (statusCode == 1) {
			Toast.makeText(this, "Server not Responding ", Toast.LENGTH_SHORT).show();
		} else if (statusCode == 0) {
			Intent intent = new Intent(getApplicationContext(),
					HomeActivity.class);
			intent.putExtra("Uid", StringUtils.UID);
			startActivity(intent);
		}

		progressDialog.hide();

	}
}
