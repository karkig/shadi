package com.shadiconnect;

import java.util.ArrayList;
import java.util.List;

import com.shadi.dao.ShadiDetails;
import com.shadi.dao.ShadiEvents;

public class ShadiAdapterData {
	
	ShadiDetails shadiDetails;
	ArrayList<ShadiEvents> list;
	
	public ShadiAdapterData(ShadiDetails shadi,ArrayList<ShadiEvents>eventList)
	{
		shadiDetails = shadi;
		list = eventList;
	}
	
	public ShadiDetails getShadiDetails() {
		return shadiDetails;
	}
	public void setShadiDetails(ShadiDetails shadiDetails) {
		this.shadiDetails = shadiDetails;
	}
	public ArrayList<ShadiEvents> getList() {
		return list;
	}
	public void setList(ArrayList<ShadiEvents> list) {
		this.list = list;
	}

	
	
	
}
