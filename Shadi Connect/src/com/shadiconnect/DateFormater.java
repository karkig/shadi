package com.shadiconnect;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import android.annotation.SuppressLint;

/**
 */
public class DateFormater {
	static DateFormat readFormat = null; // Dec
	static DateFormat writeFormat = null;
	static DateFormat timeWriteFormat =null;
	static Date date = null;
	public static Date readDate;

	@SuppressLint("SimpleDateFormat")
	public static String getDate(String s) {
		try {
			readFormat = new SimpleDateFormat("MMM dd yyyy h:mma");
			writeFormat = new SimpleDateFormat("dd-MMM-yyyy");
			date = readFormat.parse(s);
			String formattedDate = "";
			if (date != null) {
				formattedDate = writeFormat.format(date);
				return formattedDate;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@SuppressLint("SimpleDateFormat")
	public static String getTime(String s)
	{
		
		try {
			readFormat = new SimpleDateFormat("MMM dd yyyy h:mma");
			timeWriteFormat = new SimpleDateFormat("h:mm a");
			date = readFormat.parse(s);
			String formattedDate = "";
			if (date != null) {
				formattedDate = timeWriteFormat.format(date);
				return formattedDate;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
