package com.shadiconnect;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.shadi.dao.DaoMaster;
import com.shadi.dao.DaoSession;

import android.app.Application;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.shaadiconnect.*;

/**
 * This is used for connecting with the database.
 */
public class DBConnection extends Application {
	public static DisplayImageOptions options;
	private static SQLiteDatabase db;
	private static DaoMaster daoMaster;
	private static DaoSession daoSession;

	public static DaoSession getDaoSession() {
		return daoSession;
	}

	public static void resetData() {
		try {
			DaoMaster.dropAllTables(db, true);
		} catch (Exception e) {
		}
	}

	public static void createTables() {
		try {
			DaoMaster.createAllTables(db, true);
		} catch (Exception e) {

		}
	}

	public static Boolean checkForInternetConnection(Context context) {
		final ConnectivityManager conMgr = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		final NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
		if (activeNetwork != null && activeNetwork.isConnected()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void onCreate() {
		super.onCreate();
		try {
			DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this,
					"shadi-db", null);
			db = helper.getWritableDatabase();
			daoMaster = new DaoMaster(db);
			init();

			daoSession = daoMaster.newSession();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void init() {
		options = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.profile_blank)
				.showImageForEmptyUri(R.drawable.profile_blank)
				// .showImageOnFail(R.drawable.chat_profile_icon)
				.cacheInMemory(true).cacheOnDisc(true)
				.imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.displayer(new FadeInBitmapDisplayer(1500)).build();

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
				this).tasksProcessingOrder(QueueProcessingType.LIFO)
				.denyCacheImageMultipleSizesInMemory()
				.defaultDisplayImageOptions(options).writeDebugLogs().build();
		ImageLoader.getInstance().init(config);
	}

}