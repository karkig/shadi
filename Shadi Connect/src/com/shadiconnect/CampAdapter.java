package com.shadiconnect;

import android.R.layout;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import com.shaadiconnect.*;

public class CampAdapter extends ArrayAdapter<ContactData> {
	Context ctx;
	private Activity activity;
	private List<ContactData> items;

	public CampAdapter(Context context, int layoutResourceId,
			List<ContactData> data) {
		super(context, R.layout.contact_item, data);
		ctx = context;
		this.activity = (Activity) context;
		this.items = data;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;

		if (convertView == null) {
			holder = new ViewHolder();
            
			LayoutInflater inflater = activity.getLayoutInflater();
			convertView = inflater.inflate(R.layout.contact_item, null);
			holder.layout=(LinearLayout) convertView.findViewById(R.id.mainlayout);
			
		/*	holder.contactName = (TextView) convertView
					.findViewById(R.id.name_textview);*/
			holder.contactNumber = (TextView) convertView
					.findViewById(R.id.phone_no);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		try {
			final ContactData data = items.get(position);
		//	holder.contactName.setText(data.getContactName());
			holder.contactNumber.setText(data.getPhoneNo());
			
			holder.layout.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					Intent intent = new Intent(Intent.ACTION_DIAL);
					String no = items.get(position).getPhoneNo();
					intent.setData(Uri.parse("tel:"+no));
					activity.startActivity(intent); 
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
		return convertView;
	}

	public void alertBox(final String status, final String userid) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);

		alertDialogBuilder.setTitle("Alert");

		alertDialogBuilder.setMessage("Do You want update status?")
				.setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

					}
				});

		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}

	public void addAll(List<ContactData> destinations) {
		this.items.addAll(destinations);
		notifyDataSetChanged();
	}

	public void clear() {
		this.items.clear();
	}

	private static class ViewHolder {

		TextView contactName, contactNumber;
		LinearLayout layout;
	}

}