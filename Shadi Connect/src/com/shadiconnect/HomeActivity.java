package com.shadiconnect;

import java.util.List;

import com.coverflow.CoverFlowEvent;
import com.coverflow.CoverFlowGallery;
import com.coverflow.CoverFlowShadi;
import com.shadi.dao.ShadiDetails;
import com.shadi.dao.ShadiPhotos;
import com.shadi.network.CampRestClientUsage;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import com.shadiconnect.*;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.shaadiconnect.*;

public class HomeActivity extends Activity implements OnClickListener {
	private LinearLayout crousalBtn1;
	private LinearLayout galleryBtn;
	private LinearLayout contactBtn;
	private LinearLayout venueBtn;
	TextView name_slip;
	String Uid;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_home);

		Uid = getIntent().getStringExtra("Uid");

		StringUtils.Shadi_Id = Uid;
		name_slip = (TextView) findViewById(R.id.name_slip);
		crousalBtn1 = (LinearLayout) findViewById(R.id.buttonEvent);
		crousalBtn1.setOnClickListener(this);
		galleryBtn = (LinearLayout) findViewById(R.id.buttonGallery);
		galleryBtn.setOnClickListener(this);
		contactBtn = (LinearLayout) findViewById(R.id.buttonContact);
		contactBtn.setOnClickListener(this);
		venueBtn = (LinearLayout) findViewById(R.id.venu_button);
		venueBtn.setOnClickListener(this);
		
		ShadiDetails sd = DBUtils.getShadiDetails(StringUtils.Shadi_Id);
		name_slip.setText(sd.getBrideName()+"  Weds  "+sd.getGroomName());
	}

	@Override
	public void onClick(View v) {
		Intent intent;
		final Context context = this;

		switch (v.getId()) {
		case R.id.buttonEvent:

			intent = new Intent(HomeActivity.this, CoverFlowEvent.class);
			startActivity(intent);
			break;
		case R.id.buttonGallery:

			List<ShadiPhotos> list = DBUtils
					.getShadiPhotos(StringUtils.Shadi_Id);
			if (list.size() <= 0) {
				Toast.makeText(this, "Gallery Empty ", Toast.LENGTH_SHORT)
						.show();
			} else {
				Intent i = new Intent(HomeActivity.this, CoverFlowGallery.class);
				startActivity(i);
			}

			break;
		case R.id.buttonContact:
			intent = new Intent(context, ContactActivity.class);
			startActivity(intent);
			break;
		case R.id.venu_button:
			intent = new Intent(HomeActivity.this, VenuNewActivity.class);
			startActivity(intent);
			break;

		}

	}
}
