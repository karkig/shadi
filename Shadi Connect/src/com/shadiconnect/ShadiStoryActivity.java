package com.shadiconnect;

import com.shadi.dao.ShadiDetails;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import com.shaadiconnect.*;

public class ShadiStoryActivity extends Activity {
	TextView story_text;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.shadi_story);
		init();
		ShadiDetails details = DBUtils.getShadiDetails(StringUtils.Shadi_Id);
		story_text.setText(details.getShadiStory());

	}

	public void init() {
		story_text = (TextView) findViewById(R.id.story_text);

	}
}
