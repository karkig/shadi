package com.shadiconnect;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.shaadiconnect.*;
import at.technikum.mti.fancycoverflow.FancyCoverFlowAdapter;

public class EventListAdapter extends FancyCoverFlowAdapter {
	Context ctx;
	ArrayList<String> data;
	private Activity activity;

	public EventListAdapter(Context context, int layoutResourceId,
			List<String> data) {
		ctx = (Activity) context;
		this.data = (ArrayList<String>) data;
		this.activity=(Activity) context;
		// TODO Auto-generated constructor stub
	}

	@Override
	public int getCount() {
		return data.size();
	}	

	@Override
	public long getItemId(int i) {
		return i;
	}

	@Override
	public View getCoverFlowItem(int i, View reuseableView, ViewGroup viewGroup) {
		
			LayoutInflater inflater = activity.getLayoutInflater();
			reuseableView = inflater.inflate(R.layout.activity_contact, viewGroup);
		

		return viewGroup;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}


}
